<?php

namespace app\commands;

use app\repositories\ContactRepository;
use Faker\Factory;
use yii\console\Controller;

/**
 * To manage test data
 * Class FakerController
 * @package app\commands
 */
class FakerController extends Controller
{
    /**
     * Generates test data for contacts
     * ```
     * ./yii faker/populate-contacts
     * ```
     *
     * @param int $size
     */
    public function actionPopulateContacts($size = 1000)
    {
        $faker = Factory::create();
        $count = 0;
        $contacts = [];
        while ($count < $size) {
            $count ++;
            $contacts[] = [
                'name' => $faker->name(),
                'email' => $faker->unique()->email,
            ];
        }
        ContactRepository::bulkInsert($contacts);
    }
}