<?php

namespace app\commands;

use app\models\User;
use yii\console\Controller;

/**
 * Manages users
 * Class UserController
 * @package app\commands
 */
class UserController extends Controller
{
    /**
     * Creates new user
     * ./yii user/create admin admin@test.com admin
     * @param $username
     * @param $email
     * @param $password
     */
    public function actionCreate($username, $email, $password)
    {
        $user = new User();
        $user->email = $email;
        $user->username = $username;
        $user->status = User::STATUS_ACTIVE;
        $user->setPassword($password);
        $user->generateAuthKey();
        $user->save();

        echo "User generated id : $user->id" . PHP_EOL;
    }
}