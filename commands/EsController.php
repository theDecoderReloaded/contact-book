<?php

namespace app\commands;

use app\models\Contact;
use app\services\ContactService;
use Yii;
use yii\console\Controller;

/**
 * Manages elastic search from console
 * Class EsController
 * @package app\commands
 */
class EsController extends Controller
{
    /**
     * Loads data to ES for contact model
     * ```
     * ./yii es/load-contacts-data
     * ```
     */
    public function actionLoadContactsData()
    {
        $id = 1;
        $contact = true;
        while ($contact) {
            $contact = Contact::findOne($id);
            if (!$contact) {
                break;
            }
            Yii::$app->modelSaveLogger->log($contact->getEsAttributes());
            echo "Pushed $id" . PHP_EOL;
            $id++;
        }
    }

    public function actionTest($field, $query)
    {
        print_r(ContactService::search($field, $query, 1));
    }
}