<?php

namespace app\services;

use app\helpers\Search;
use app\modules\v1\models\Contact;

class ContactService
{
    public static function search($field, $query, $page)
    {
        $params = [
            'model' => Contact::ES_MODEL_NAME,
            'collectionEnvelope' => Contact::COLLECTION_ENVELOPE,
            'searchAttribute' => $field,
            'searchString' => $query,
            'sortField' => 'timestamp',
            'sortOrder' => 'DESC',
            'searchType' => 'doc',
            'sourceAttributes' => Contact::ES_ATTRIBUTES,
            'page' => $page
        ];
        return Search::es($params);
    }
}