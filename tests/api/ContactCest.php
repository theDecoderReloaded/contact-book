<?php

use app\fixtures\ContactFixture;
use app\fixtures\UserFixture;
use app\modules\v1\models\Contact;
use app\tests\ApiTester;

class ContactCest
{
    /**
     * @var \app\tests\UnitTester
     */
    protected $tester;

    private $url = '/contacts';

    /**
     * @inheritdoc
     */
    public function _fixtures()
    {
        return [
            'contact' => [
                'class' => ContactFixture::class
            ],
            'user' => [
                'class' => UserFixture::class
            ],
        ];
    }

    public function testGetFound(ApiTester $I)
    {
        $url = $this->url . '/1';
        $I->amHttpAuthenticated('admin', 'admin');
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGET($url);

        $I->seeResponseCodeIs(200);
        $I->seeResponseJsonMatchesJsonPath('id');
        $I->seeResponseJsonMatchesJsonPath('email');
        $I->seeResponseJsonMatchesJsonPath('name');

        $I->canSeeResponseMatchesJsonType([
            'id'=> 'integer',
            'email'=> 'string',
            'name'=> 'string',
            'created_at'=> 'string',
            'updated_at'=> 'string',
        ]);
    }

    public function testGetNotFound(ApiTester $I)
    {
        $url = $this->url . '/100';
        $I->amHttpAuthenticated('admin', 'admin');
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGET($url);

        $I->seeResponseCodeIs(404);
    }

    public function testGetList(ApiTester $I)
    {
        $url = $this->url;
        $collectionEnvelop = Contact::COLLECTION_ENVELOPE;
        $I->amHttpAuthenticated('admin', 'admin');
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGET($url);

        $I->seeResponseCodeIs(200);
        $I->seeResponseJsonMatchesJsonPath("$.$collectionEnvelop");
        $I->seeResponseJsonMatchesJsonPath("$.{$collectionEnvelop}[0].id");
        $I->seeResponseJsonMatchesJsonPath("$.{$collectionEnvelop}[0].email");
        $I->seeResponseJsonMatchesJsonPath("$.{$collectionEnvelop}[0].name");

        $I->canSeeResponseMatchesJsonType([
            'id'=> 'integer',
            'email'=> 'string',
            'name'=> 'string',
            'created_at'=> 'string',
            'updated_at'=> 'string',
        ], "$.{$collectionEnvelop}[0]");
    }

    public function testPostValid(ApiTester $I)
    {

    }

    public function testPostInvalid(ApiTester $I)
    {

    }

    public function testPutValid(ApiTester $I)
    {

    }

    public function testPutInvalid(ApiTester $I)
    {

    }

    public function testDeleteValid(ApiTester $I)
    {

    }

    public function testDeleteInvalid(ApiTester $I)
    {

    }
}