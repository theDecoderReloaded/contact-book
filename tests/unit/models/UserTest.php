<?php

namespace tests\models;

use app\fixtures\UserFixture;

class UserTest extends \Codeception\Test\Unit
{
    /**
     * @var \app\tests\UnitTester
     */
    protected $tester;

    /**
     * @inheritdoc
     */
    public function _fixtures()
    {
        return [
            'contact' => [
                'class' => UserFixture::class
            ],
        ];
    }
}