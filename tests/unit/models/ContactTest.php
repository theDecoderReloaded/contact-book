<?php

namespace tests\models;
use app\fixtures\ContactFixture;
use app\models\Contact;

class ContactTest extends \Codeception\Test\Unit
{
    /**
     * @var \app\tests\UnitTester
     */
    protected $tester;

    /**
     * @inheritdoc
     */
    public function _fixtures()
    {
        return [
            'contact' => [
                'class' => ContactFixture::class
            ],
        ];
    }
}