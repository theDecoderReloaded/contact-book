<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contact".
 *
 * @property int $id
 * @property string $email
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 */
class Contact extends \yii\db\ActiveRecord
{
    const ES_MODEL_NAME = 'contact';

    const ES_ATTRIBUTES = ['email', 'name',];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['email', 'name'], 'string', 'max' => 255],
            [['email'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'name' => 'Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Generates attribute array to save in elasticsearch index 'model-contact'
     * @return array
     */
    public function getEsAttributes()
    {
        return [
            'timestamp' => date('c', strtotime($this->created_at)),
            'doc_id' => $this->id,
            'model_name' => self::ES_MODEL_NAME,
            'name' => $this->name,
            'email' => $this->email,
        ];
    }

    /**
     * Actions after save:
     * (1) Saves Contact data in elasticSearch.
     * @param bool  $insert
     * @param array $attributes
     */
    public function afterSave($insert, $attributes)
    {
        Yii::$app->modelSaveLogger->log($this->getEsAttributes());

        parent::afterSave($insert, $attributes);
    }
}
