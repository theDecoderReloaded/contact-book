<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m180921_103059_create_table_contacts
 */
class m180921_103059_create_table_contact extends Migration
{
    public $table = '{{%contact}}';
    public function up()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'email' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'created_at' => Schema::TYPE_TIMESTAMP . ' not null default current_timestamp',
            'updated_at' => Schema::TYPE_TIMESTAMP . ' default current_timestamp on update current_timestamp',
        ]);

        $this->createIndex('idx-contact-email', $this->table, ['email'], true);
        $this->createIndex('idx-contact-name', $this->table, ['name']);
    }

    public function down()
    {
        $this->dropTable($this->table);
    }
}
