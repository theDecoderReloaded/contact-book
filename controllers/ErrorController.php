<?php

namespace app\controllers;

use Yii;
use yii\base\Controller;
use yii\web\Response;

/**
 * Class ErrorController
 * @package app\controllers
 */
class ErrorController extends Controller
{
    /**
     * Custom method for rendering error msg
     * @return array
     */
    public function actionIndex()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $exception = Yii::$app->errorHandler->exception;
        return [
            'message' => $exception->getMessage()
        ];
    }
}