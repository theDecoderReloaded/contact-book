<?php

namespace app\modules\v1\controllers;

use app\models\User;
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;

/**
 * Base class for all Api controllers
 * Class ApiController
 * @package app\modules\v1\controllers
 */
class ApiController extends ActiveController
{
    public function init()
    {
        parent::init();
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::class,
            'auth' => [$this, 'auth']
        ];
        return $behaviors;
    }

    public function auth($username, $password)
    {
        $user = User::findByUsername($username);

        return $user->validatePassword($password) ? $user : null;
    }
}