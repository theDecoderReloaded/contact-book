<?php

namespace app\modules\v1\controllers;

use app\modules\v1\models\Contact;
use app\services\ContactService;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;

/**
 * Class for contact rest API
 * Class ContactController
 * @package app\modules\v1\controllers
 */
class ContactController extends ApiController
{
    public $modelClass = Contact::class;
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => Contact::COLLECTION_ENVELOPE,
    ];

    /**
     * Provides response for search
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionSearch()
    {
        $params = Yii::$app->request->get();
        $query = ArrayHelper::getValue($params, 'q', '');
        $field = ArrayHelper::getValue($params, 'field');
        $page = max((int) ArrayHelper::getValue($params, 'page', 1), 1);
        if (!in_array($field, Contact::ES_ATTRIBUTES)) {
            throw new BadRequestHttpException('Bad Query Parameters');
        }

        return ContactService::search($field, $query, $page);
    }
}