<?php

namespace app\helpers;

use Yii;

class Search
{
    const MAX_PAGE_SIZE = 10;

    public static function es($params)
    {
        $model = $params['model'];
        $collectionName = $params['collectionEnvelope'];
        $searchString = $params['searchString'];
        $sortField = $params['sortField'];
        $searchAttribute = $params['searchAttribute'];
        $sortOrder = $params['sortOrder'];
        $type = $params['searchType'];
        $attributes = array_merge(['doc_id', 'timestamp'], $params['sourceAttributes']);
        $page = $params['page'];
        $searchStringArray = explode('@', $searchString, 2);
        $searchString = $searchStringArray[0];
        $output = [];
        $totalRecord = 0;
        $index = 'model-'.$model;
        $searchBody = [
            'query' => [
                'bool' => [
                    'must' => [
                        'query_string' => [
                            'query' => "*$searchString*",
                            'fields' => [$searchAttribute],
                        ],
                    ],
                ],
            ],
            'size' => self::MAX_PAGE_SIZE,
            '_source' => $attributes,
            'sort' => [
                $sortField => ['order' => $sortOrder]
            ],
            'from' => (int) ($page - 1) * self::MAX_PAGE_SIZE,
        ];

        $records = Yii::$app->es->search($index, $type, $searchBody);
        if (isset($records['hits']['total'])) {
            $totalRecord = $records['hits']['total'];
            foreach ($records['hits']['hits'] as $record) {
                $record['_source']['id']= $record['_source']['doc_id'];
                unset($record['_source']['doc_id']);
                $output[] = $record['_source'];
            }
        }

        return [
            $collectionName => $output,
            '_meta' => self::getMeta($totalRecord, $page)
        ];
    }

    private static function getMeta($totalRecord, $currentPage)
    {
        $totalPages = ceil( $totalRecord / self::MAX_PAGE_SIZE);
        $meta['totalCount'] = $totalRecord;
        $meta['totalPages'] = $totalPages;
        $meta['currentPage'] = $currentPage;
        $prevPage =  max($currentPage - 1, 1);
        if ($totalRecord && $prevPage != $currentPage) {
            $meta['prevPage'] = $prevPage;
        }
        $nextPage = min($currentPage + 1, $totalPages);
        if ($totalRecord && $nextPage != $totalPages) {
            $meta['nextPage'] = $nextPage;
        }
        $meta['perPage'] = self::MAX_PAGE_SIZE;

        return $meta;
    }
}
