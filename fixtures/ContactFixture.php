<?php

namespace app\fixtures;

use app\models\Contact;
use yii\test\ActiveFixture;

class ContactFixture extends ActiveFixture
{
    public $modelClass = Contact::class;
}