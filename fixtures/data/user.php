<?php
return [
    'user1' => [
        'id' => 1,
        'username' => 'admin',
        'auth_key' => 'n6ithGsDCq_E-j2ojXeAM4ch6prckbHK',
        'password_hash' => '$2y$13$JxzYDTPlRTG2ldCHBKOSuew6iWPeKvrKRKOmKjaZIWcoU4M86PTPy',
        'email' => 'admin@test.com',
        'status' => 10,
        'created_at' => '2018-09-24 00:21:09',
        'updated_at' => '2018-09-24 00:21:09',
    ]
];