<?php

namespace app\repositories;

use app\models\Contact;
use Yii;

/**
 * Layer between model and service
 * Class ContactRepository
 * @package app\repositories
 */
class ContactRepository
{
    public static function bulkInsert($contacts)
    {
        $dbObject = Yii::$app->db;
        $bulkInsertCommand = $dbObject->createCommand()->batchInsert(
            Contact::tableName(),
            ['name', 'email'],
            $contacts
        );
        $bulkInsertCommandRawSql = $bulkInsertCommand->rawSql;
        $bulkInsertCommandRawSql = str_replace('INSERT', 'INSERT IGNORE', $bulkInsertCommandRawSql);

        return $dbObject->createCommand($bulkInsertCommandRawSql)->execute();
    }
}