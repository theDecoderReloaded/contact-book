<?php

namespace app\components;

use app\components\log\ModelSaveFileTarget;
use Yii;
use yii\base\Component;

/**
 * Component for logging model data
 * Class ModelSaveLogger
 * @package app\components
 */
class ModelSaveLogger extends Component
{

    /**
     * Logs model data to file target
     * @param $parameters
     */
    public function log($parameters)
    {
        $message[1] = ModelSaveFileTarget::LEVEL_MODEL_SAVE;
        $message[0] = $parameters;
        $message[3] = microtime(true);

        Yii::$app->log->dispatch([$message], false);
    }
}
