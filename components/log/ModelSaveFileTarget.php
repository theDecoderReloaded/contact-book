<?php

namespace app\components\log;

class ModelSaveFileTarget extends LogstashFileTarget
{
    const LEVEL_MODEL_SAVE = 0x200;
    const LEVEL_NAME = 'modelSave';

    /**
     * Parses the model parameters and returns all the variables as an array.
     *
     * @param array $message model fields
     *
     * @return array the message details array
     */
    public function formatMessage($message)
    {
        list($parameters) = $message;

        return $parameters;
    }

    public function getLevels()
    {
        return self::LEVEL_MODEL_SAVE;
    }
}
