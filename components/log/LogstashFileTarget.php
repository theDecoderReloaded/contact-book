<?php

namespace app\components\log;

use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\VarDumper;
use yii\log\FileTarget;
use yii\web\Request;


class LogStashFileTarget extends FileTarget
{
    /**
     * the key in the messages array which contains the context information.
     */
    const KEY_LOG_VARS = 'logVars';

    /**
     * Processes the given log messages.
     * This method will filter the given messages with [[levels]] and [[categories]].
     * And if requested, it will also export the filtering result to specific medium (e.g. email).
     * Only change from the parent implementation is the inclusion of logVars as a separate key
     * in the array rather than a completely separate element.
     *
     * @param array $messages log messages to be processed. See [[Logger::messages]] for the structure
     *                        of each message.
     * @param bool  $final    whether this method is called at the end of the current application
     */
    public function collect($messages, $final)
    {
        $this->messages = array_merge($this->messages, $this->filterMessages($messages, $this->getLevels(), $this->categories, $this->except));

        $count = count($this->messages);

        if ($count > 0 && ($final || $this->exportInterval > 0 && $count >= $this->exportInterval)) {
            if (($context = $this->getContextMessage()) !== '') {
                // this is the only change from the parent implementation
                $this->messages[self::KEY_LOG_VARS] = $context;
            }

            // set exportInterval to 0 to avoid triggering export again while exporting
            $oldExportInterval = $this->exportInterval;
            $this->exportInterval = 0;

            $this->export();

            $this->exportInterval = $oldExportInterval;

            $this->messages = [];
        }
    }

    /**
     * Writes log messages to the file.
     *
     * @throws InvalidConfigException if unable to open the log file for writing
     */
    public function export()
    {
        // taking the first element as the error message will always be the first element
        $message = $this->formatMessage($this->messages[0]);

        // processing the log vars
        foreach ($this->messages[self::KEY_LOG_VARS] as $key => $value) {
            $message[$key] = $value;
        }

        // encoding as JSON
        $text = json_encode($message)."\n";

        // rest implementation is same as the parent's
        if (($fp = @fopen($this->logFile, 'a')) === false) {
            throw new InvalidConfigException("Unable to append to log file: {$this->logFile}");
        }
        @flock($fp, LOCK_EX);
        if ($this->enableRotation) {
            // clear stat cache to ensure getting the real current file size and not a cached one
            // this may result in rotating twice when cached file size is used on subsequent calls
            clearstatcache();
        }
        if ($this->enableRotation && @filesize($this->logFile) > $this->maxFileSize * 1024) {
            $this->rotateFiles();
            @flock($fp, LOCK_UN);
            @fclose($fp);
            @file_put_contents($this->logFile, $text, FILE_APPEND | LOCK_EX);
        } else {
            @fwrite($fp, $text);
            @flock($fp, LOCK_UN);
            @fclose($fp);
        }
        if ($this->fileMode !== null) {
            @chmod($this->logFile, $this->fileMode);
        }
    }

    /**
     * Generates the context information to be logged.
     * The default implementation will dump user information, system variables, etc.
     * Returning an array instead of string.
     *
     * @return array the context information. If an empty array, it means no context information.
     */
    public function getContextMessage()
    {
        $context = [];
        foreach ($this->logVars as $name) {
            if (!empty($GLOBALS[$name])) {
                $context["\${$name}"] = VarDumper::dumpAsString($GLOBALS[$name]);
            }
        }

        return $context;
    }

    /**
     * Returns user IP, user ID and session ID.
     *
     * @param array $message the message being exported.
     *                       The message structure follows that in [[Logger::messages]].
     *
     * @return array the details array
     */
    public function getUserDetails($message)
    {
        if ($this->prefix !== null) {
            return call_user_func($this->prefix, $message);
        }

        if (Yii::$app === null) {
            return '';
        }

        $request = Yii::$app->getRequest();
        $ip = $request instanceof Request ? $request->getUserIP() : '-';

        /* @var $user \yii\web\User */
        $user = Yii::$app->has('user', true) ? Yii::$app->get('user') : null;
        if ($user && ($identity = $user->getIdentity(false))) {
            $userID = $identity->getId();
        } else {
            $userID = '-';
        }

        /* @var $session \yii\web\Session */
        $session = Yii::$app->has('session', true) ? Yii::$app->get('session') : null;
        $sessionID = $session && $session->getIsActive() ? $session->getId() : '-';

        return [
            'ip' => $ip,
            'userId' => (string) $userID,
            'sessionId' => $sessionID,
        ];
    }

    /**
     * Overriding the parent implementation to remove all other message levels.
     *
     * @param array|integer $levels message levels that this target is interested in.
     */
    public function setLevels($levels)
    {
    }
}
