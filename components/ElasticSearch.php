<?php

namespace app\components;

use Elasticsearch\ClientBuilder;
use yii\base\Component;

class ElasticSearch extends Component
{
    /**
     * the client object.
     * @var \Elasticsearch\ClientBuilder
     */
    private $_client;

    /**
     * list of elasticsearch hosts.
     * @var array
     */
    public $hosts = ['localhost:9200'];

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        $this->_client = ClientBuilder::create()->setHosts($this->hosts)->build();
    }

    /**
     * Indexes a new document.
     * @param string $index the index name
     * @param string $type  the document type
     * @param array  $body  the document body
     */
    public function add($index, $type, $body)
    {
        return $this->_client->index([
            'index' => $index,
            'type' => $type,
            'body' => $body,
        ]);
    }

    /**
     * Retrieves a document.
     * @param string $index the index name
     * @param string $type  the document type
     * @param string $id    the document id
     * @return array the document
     */
    public function get($index, $type, $id)
    {
        return $this->_client->get([
            'index' => $index,
            'type' => $type,
            'id' => $id,
        ]);
    }

    /**
     * Searches a document.
     * @param string $index the index name
     * @param string $type  the document type
     * @param array  $body  the search parameters
     * @return array
     */
    public function search($index, $type, $body)
    {
        return $this->_client->search([
            'index' => $index,
            'body' => $body,
            'type' => $type,
            'client' => [
                'timeout' => 1,
                'connect_timeout' => 1
            ]
        ]);
    }

    /**
     * Deletes a document.
     * @param string $index the index name
     * @param string $type  the document type
     * @param string $id    the document id
     * @return array
     */
    public function delete($index, $type, $id)
    {
        return $this->_client->delete([
            'index' => $index,
            'type' => $type,
            'id' => $id,
        ]);
    }

    /**
     * Creates an index.
     * @param string $index the index name
     * @param array  $body  the parameters
     * @return array
     */
    public function createIndex($index, $body)
    {
        return $this->_client->indices()->create([
            'index' => $index,
            'body' => $body,
        ]);
    }

    /**
     * Deletes an index.
     * @param string $index the index name
     * @return array
     */
    public function deleteIndex($index)
    {
        return $this->_client->indices()->delete(['index' => $index]);
    }

    /**
     * Indexes a new document.
     * @param string $index the index name
     * @param string $type  the document type
     * @param array  $body  the document body
     */
    public function addWithId($index, $type, $id, $body)
    {
        return $this->_client->index([
            'index' => $index . '-' . date('Y.m.d'),
            'type' => $type,
            'id' => $id,
            'body' => $body,
        ]);
    }

    /**
     * Check health of the ES
     * @param None
     */
    public function ping()
    {
        return $this->_client->ping();
    }
}
