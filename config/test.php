<?php
/**
 * Application configuration shared by all test types
 */
$config = yii\helpers\ArrayHelper::merge(
    require __DIR__ . '/web.php',
    [
        'id' => 'api-tests',
        'basePath' => dirname(__DIR__),
        'aliases' => [
            '@bower' => '@vendor/bower-asset',
            '@npm'   => '@vendor/npm-asset',
        ],
        'language' => 'en-US',
        'components' => [
            'db' => require __DIR__ . '/test_db.php',
            'mailer' => [
                'useFileTransport' => true,
            ],
            'assetManager' => [
                'basePath' => __DIR__ . '/../web/assets',
            ],
            'urlManager' => [
                'showScriptName' => true,
            ],
            'user' => [
                'class' => 'yii\web\User',
                'identityClass' => 'app\modules\v1\models',
            ],
            'request' => [
                'cookieValidationKey' => 'test',
                'enableCsrfValidation' => false,
                // but if you absolutely need it set cookie domain to localhost
                /*
                'csrfCookie' => [
                    'domain' => 'localhost',
                ],
                */
            ],
        ],
        'params' => require __DIR__ . '/params.php',
    ]
);


return $config;